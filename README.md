# SmartRanch
Base hub for sensor data read. Collected data is pushed to MQTT server using available network connection (see branch map below). Please note that GPRS imlementation is now abandonned.

Reading data at this stage:
- temperature/humidity/pressure - using BME280 sensor
- AC voltage - using custom made module with transformer

Current implementation depend on local WiFi hotspot powered by DIY UPS with MCP73871, one or two 18650 batteries and two USB-A DC-DC buck converters >= 1200mA.

code_secrets.h file is excrluded from commits. 

Copy  
__examples/code_secrets-example.h__  
to  
__src/code_secrets.h__  
and set correct values

IMPORTANT!  
Take user and pass from HomeAssistant integration (MQTT configuration) and  
set same credentials in MQTT add-on configuration, or Home assistant will be  
not able to read topic data

## Branch map
- master - current working implementation
- weather_station_with_gprs - implementation with NodeMCU and GSM modem. Not used, archived