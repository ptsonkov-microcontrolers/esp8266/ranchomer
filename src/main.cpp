#include <Arduino.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>
#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include "code_secrets.h"
#include "code_configs.h"

// ============================================================================
// === Create instances
WiFiClient espClient;
PubSubClient mqttClient(espClient);

// ============================================================================
// === WiFi details
const char *wifi_ssid = SECRET_WIFI_SSID;
const char *wifi_pass = SECRET_WIFI_PASS;
bool networkConnection;

// ============================================================================
// === MQTT details
const char *mqtt_server = SECRET_MQTT_SERVER;
const int mqtt_port = SECRET_MQTT_PORT;
const char *mqtt_user = SECRET_MQTT_USER;
const char *mqtt_pass = SECRET_MQTT_PASS;
#define station_location "rancho"

// ============================================================================
// === Controller pins definition
#define blue_led D4

// ============================================================================
// === Sensors and measurements
#if ENABLE_BME_SENSOR_1 == true
#define ADDRESS_BME1 0x76
Adafruit_BME280 bme1;
#define topic_temp1 station_location "/out/temperature"
#define topic_humid1 station_location "/out/humidity"
#define topic_press1 station_location "/out/pressure"
#endif

#if ENABLE_BME_SENSOR_2 == true
#define ADDRESS_BME2 0x77
Adafruit_BME280 bme2;
#define topic_temp2 station_location "/in/temperature"
#define topic_humid2 station_location "/in/humidity"
#define topic_press2 station_location "/in/pressure"
#endif

#if ENABLE_AC_VOLTAGE_CHECK == true
#define topic_ac_voltage station_location "/power/ac_voltage"
#endif

#if ENABLE_DC_VOLTAGE_CHECK == true
#define topic_batt_voltage station_location "/power/battery_voltage"
#endif

float temperature1, humidity1, pressure1, temperature2, humidity2, pressure2;
uint8_t chargeState = -99;
int8_t percent = -99;
uint16_t milliVolts = -9999;

// ============================================================================
// === Timers
unsigned long deepSleepSeconds = DEEP_SLEEP_SECONDS;
unsigned long wifiConnectTimeout = WIFI_CONNECT_TIMEOUT;
unsigned long mqttConnectTimeout = MQTT_CONNECT_TIMEOUT;
unsigned long noNetworkSleepSeconds = NO_NETWORK_SLEEP_SECONDS;
unsigned long timeNow, timeDelta;

// ============================================================================
// === Forward function declaration
void sleep_now(int seconds);
bool setup_wifi();
void reconnect_mqtt();
void publish_topic(const char *topic, const char *payload);

void setup()
{

#if ENABLE_SERIAL_OUTPUT == true
#define DBG_SERIAL Serial
  DBG_SERIAL.begin(SERIAL_SPEED);
#endif

  pinMode(blue_led, OUTPUT);
  digitalWrite(blue_led, LOW);
  pinMode(A0, INPUT);

  networkConnection = setup_wifi();

  if (networkConnection)
  {
    mqttClient.setServer(mqtt_server, mqtt_port);

    if (!mqttClient.connected())
    {
      reconnect_mqtt();
    }

    mqttClient.loop();

#if ENABLE_BME_SENSOR_1 == true
    if (bme1.begin(ADDRESS_BME1))
    {
      temperature1 = bme1.readTemperature();
      humidity1 = bme1.readHumidity();
      pressure1 = bme1.readPressure() / 100.0F;
    }
    else
    {
      temperature1 = 0;
      humidity1 = 0;
      pressure1 = 0;
    }
    DBG_SERIAL.println("Temperature 1:  " + String(temperature1) + "ºC");
    DBG_SERIAL.println("Humidity 1:     " + String(humidity1) + "%");
    DBG_SERIAL.println("Pressure 1:     " + String(pressure1) + "hPa");
    publish_topic(topic_temp1, String(temperature1).c_str());
    publish_topic(topic_humid1, String(humidity1).c_str());
    publish_topic(topic_press1, String(pressure1).c_str());
#endif

#if ENABLE_BME_SENSOR_2 == true
    if (bme2.begin(ADDRESS_BME2))
    {
      temperature2 = bme2.readTemperature();
      humidity2 = bme2.readHumidity();
      pressure2 = bme2.readPressure() / 100.0F;
    }
    else
    {
      temperature2 = 0;
      humidity2 = 0;
      pressure2 = 0;
    }
    DBG_SERIAL.println("Temperature2:   " + String(temperature2) + "ºC");
    DBG_SERIAL.println("Humidity2:      " + String(humidity2) + "%");
    DBG_SERIAL.println("Pressure2:      " + String(pressure2) + "hPa");
    publish_topic(topic_temp2, String(temperature2).c_str());
    publish_topic(topic_humid2, String(humidity2).c_str());
    publish_topic(topic_press2, String(pressure2).c_str());
#endif

#if ENABLE_AC_VOLTAGE_CHECK == true
    float acVoltage;
    int zeroAnalog = 15;
    int max_read = 0;

    for (int i = 0; i < 100; i++)
    {
      int read = analogRead(A0);
      if (read > max_read)
      {
        max_read = read;
      }
      delayMicroseconds(200);
    }

    acVoltage = map(max_read - zeroAnalog, 0, 1023, 0, 250);
    DBG_SERIAL.println("RAW analog: " + String(max_read - zeroAnalog));
    DBG_SERIAL.println("AC Voltage: " + String(acVoltage) + "V");
    publish_topic(topic_ac_voltage, String(acVoltage).c_str());
#endif

#if ENABLE_DC_VOLTAGE_CHECK == true
    DBG_SERIAL.println("Battery Voltage:  " + String(milliVolts) + "mV");
    publish_topic(topic_batt_voltage, String(milliVolts).c_str());
#endif

    DBG_SERIAL.println("MQTT disconnect");
    mqttClient.disconnect();

    delay(1000);
    sleep_now(deepSleepSeconds);
  }
  else
  {
    DBG_SERIAL.println("Network failure");
    sleep_now(noNetworkSleepSeconds);
  }
}

void loop() {}

void sleep_now(int seconds)
{
  DBG_SERIAL.println("Sleep for " + String(seconds) + " seconds");
  ESP.deepSleep(seconds * 1e6);
}

bool setup_wifi()
{
  timeNow = millis() / 1000;

  DBG_SERIAL.println();
  DBG_SERIAL.print("Connecting to SSID: ");
  DBG_SERIAL.println(wifi_ssid);
  // WiFi.mode(WIFI_STA);
  WiFi.begin(wifi_ssid, wifi_pass);
  while (WiFi.status() != WL_CONNECTED)
  {
    timeDelta = millis() / 1000;
    if (timeDelta - timeNow >= wifiConnectTimeout)
    {
      break;
    }
    delay(1000);
    DBG_SERIAL.print(".");
  }

  if (WiFi.status() == WL_CONNECTED)
  {
    DBG_SERIAL.println();
    DBG_SERIAL.println("WiFi connected");
    DBG_SERIAL.println("IP address: ");
    DBG_SERIAL.println(WiFi.localIP());

    return true;
  }
  else
  {
    DBG_SERIAL.println();
    DBG_SERIAL.println("WiFi connection timeout");

    return false;
  }
}

void reconnect_mqtt()
{
  timeNow = millis() / 1000;
  while (!mqttClient.connected())
  {
    DBG_SERIAL.print("Attempting MQTT connection...");
    if (mqttClient.connect("ESP8266Client", mqtt_user, mqtt_pass))
    {
      DBG_SERIAL.println("connected");
    }
    else
    {
      timeDelta = millis() / 1000;
      if (timeDelta - timeNow >= mqttConnectTimeout)
      {
        DBG_SERIAL.println("MQTT connection timeout");
        sleep_now(deepSleepSeconds);
      }
      DBG_SERIAL.print("failed, rc=");
      DBG_SERIAL.print(mqttClient.state());
      DBG_SERIAL.println(", try again in 5 seconds");
      delay(5000);
    }
  }
}

void publish_topic(const char *topic, const char *payload)
{
  DBG_SERIAL.println("Publish topic " + String(topic));
  mqttClient.publish(topic, payload, true);
}
