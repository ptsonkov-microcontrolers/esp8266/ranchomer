// ============================================================================
// === General

// Enable or disable debug messages in console
#define ENABLE_SERIAL_OUTPUT true
// Serial speed (set same in platformio.ini)
// 115200 - normal speed
// 74880  - special speed to display debug characters of ESP8266
#define SERIAL_SPEED 115200

// Enable or disable different measurements
#define ENABLE_BME_SENSOR_1 true
#define ENABLE_BME_SENSOR_2 false
#define ENABLE_AC_VOLTAGE_CHECK true
#define ENABLE_DC_VOLTAGE_CHECK false

// ============================================================================
// === Timers

// Connecting timeout in seconds
#define WIFI_CONNECT_TIMEOUT 30
#define MQTT_CONNECT_TIMEOUT 30

// Deep Sleep in seconds
#define DEEP_SLEEP_SECONDS 600
#define NO_NETWORK_SLEEP_SECONDS 180